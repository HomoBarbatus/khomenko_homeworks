package com.example.demo_spring.services;

import com.example.demo_spring.forms.ProductForm;
import com.example.demo_spring.models.Product;
import com.example.demo_spring.repositories.ProductsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ProductsServiceImpl implements ProductsService {
    private final ProductsRepository productsRepository;

    @Autowired
    public ProductsServiceImpl(ProductsRepository productsRepository) {
        this.productsRepository = productsRepository;
    }

    @Override
    public void addProduct(ProductForm form) {
        Product product = Product.builder()
                .description(form.getDescription())
                .price(form.getPrice())
                .unit_of_measure(form.getUnit_of_measure())
                .build();

        productsRepository.save(product);
    }

    @Override
    public List<Product> getAllProducts() {
        return productsRepository.findAll();
    }

    @Override
    public void deleteProduct(Integer productID) {
        productsRepository.delete(productID);
    }

    @Override
    public Product getProduct(Integer productID) {
        return productsRepository.findById(productID) ;
    }

    @Override
    public void updateProduct (Integer productID, ProductForm form) {
        Product product = productsRepository.findById(productID);
        product.setDescription(form.getDescription());
        product.setPrice(form.getPrice());
        product.setUnit_of_measure(form.getUnit_of_measure());

        productsRepository.save(product);

    }
}
