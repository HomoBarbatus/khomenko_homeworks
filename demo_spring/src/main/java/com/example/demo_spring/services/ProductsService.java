package com.example.demo_spring.services;

import com.example.demo_spring.forms.ProductForm;
import com.example.demo_spring.models.Product;

import java.util.List;

public interface ProductsService {
    void addProduct (ProductForm form);
    List<Product> getAllProducts();
    void deleteProduct(Integer productID);
    Product getProduct(Integer productID);
    void updateProduct(Integer productID, ProductForm productForm);
}
