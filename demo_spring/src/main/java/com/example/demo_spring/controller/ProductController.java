package com.example.demo_spring.controller;

import com.example.demo_spring.forms.ProductForm;
import com.example.demo_spring.models.Product;
import com.example.demo_spring.services.ProductsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@Controller
public class ProductController {

    private ProductsService productsService;

    @Autowired
    public ProductController(ProductsService productsService) {
        this.productsService = productsService;
    }

    @GetMapping ("/products")
    public String getProductsPage(Model model) {
        List<Product> products = productsService.getAllProducts();
        model.addAttribute("products", products);
        return "products";
    }

//    @PostMapping("/products")
    @GetMapping("/products/{product-id}")
    public String getProductPage(Model model, @PathVariable ("product-id") Integer productID) {
        Product product = productsService.getProduct(productID);
        model.addAttribute("product", product);
        return "product";
    }

    public String addProduct (ProductForm form) {
        productsService.addProduct(form);
        return "redirect:/products";
    }

    //post-запрос на локал хост с указанием id товара localhost/products/1/delete
    @PostMapping("/products/{product-id}/delete")
    public String deleteProduct(@PathVariable ("product-id") Integer productID) {
        productsService.deleteProduct(productID);
        return "redirect:/products";
    }

    @PostMapping("/products/{product-id}/update")
    public String update(@PathVariable ("product-id") Integer productID, ProductForm productForm) {
        productsService.updateProduct(productID, productForm);
        return "redirect:/products/update";
    }



}

