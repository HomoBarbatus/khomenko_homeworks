package com.example.demo_spring.repositories;

import com.example.demo_spring.models.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.List;

@Component
public class ProductsRepositoryJDBCImpl implements ProductsRepository {

    //language = SQL
    private static final String SQL_INSERT = "insert into item(description, price, unit_of_measure) values(?, ?, ?)";
    private static final String SQL_SELECT_ALL = "select * from item";
    private static final String SQL_SELECT_BY_PRICE = "SELECT * FROM item WHERE price = ?";
    private static final String SQL_SELECT_BY_ORDER_COUNT = "SELECT item.id, item.description, item.price, item.unit_of_measure, count(item.id) FROM item " +
            "JOIN purchase_order ON purchase_order.item_id = item.id " +
            "GROUP BY item.id " +
            "HAVING count(*) = ?";
    private static final String SQL_DELETE_BY_ID = "DELETE FROM item WHERE item.id = ?";
    private static final String SQL_SELECT_BY_ID = "SELECT * FROM item WHERE item.id = ?";
    private static final String SQL_UPDATE_BY_ID = "UPDATE item SET item.description = ?, item.price = ?, item.unit_of_measure = ?, WHERE item.id = ?";

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public ProductsRepositoryJDBCImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    private static final RowMapper<Product> productRowMapper = (row, rowNumber) -> {
        int id = row.getInt("id");
        String description = row.getString("description");
        double price = row.getDouble("price");
        String unit_of_measure = row.getString("unit_of_measure");

        return new Product(id, description, price, unit_of_measure);
    };


    @Override
    public void save(Product product) {
        jdbcTemplate.update(SQL_INSERT, product.getDescription(), product.getPrice(), product.getUnit_of_measure());
    }

    @Override
    public List<Product> findAll() {
        return jdbcTemplate.query(SQL_SELECT_ALL, productRowMapper);
    }

    @Override
    public List<Product> findAllByPrice(double price) {
        return jdbcTemplate.query(SQL_SELECT_BY_PRICE, productRowMapper, price);
    }

    @Override
    public List<Product> findAllByOrdersCount(int ordersCount) {
        return jdbcTemplate.query(SQL_SELECT_BY_ORDER_COUNT, productRowMapper, ordersCount);
    }

    @Override
    public void delete(Integer productID) {
        jdbcTemplate.update(SQL_DELETE_BY_ID, productID);
    }

    @Override
    public Product findById(Integer productID) {
        return jdbcTemplate.queryForObject(SQL_SELECT_BY_ID, productRowMapper, productID);
    }

    @Override
    public void updateById(Product product) {
        jdbcTemplate.update(SQL_UPDATE_BY_ID, product.getDescription(), product.getPrice(), product.getUnit_of_measure(), product.getId());
    }

}
