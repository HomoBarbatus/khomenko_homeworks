package com.example.demo_spring.repositories;

import com.example.demo_spring.models.Product;

import java.util.List;

public interface ProductsRepository {

void save(Product product);
List<Product> findAll();
List<Product> findAllByPrice(double price);
List<Product> findAllByOrdersCount(int ordersCount); // найти все товары по количеству заказов, в которых участвуют
void delete (Integer productID);
Product findById(Integer productID);
void updateById(Product product);
}
