package com.example.demo_spring.forms;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ProductForm {
    private String description;
    private Double price;
    private String unit_of_measure;

}
